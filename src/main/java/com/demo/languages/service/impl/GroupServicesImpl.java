package com.demo.languages.service.impl;


import com.demo.languages.dto.Request.GroupServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.GroupServicesOutput;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.dto.Response.ServicesOutput;
import com.demo.languages.entities.GroupServicesEntity;
import com.demo.languages.exception.RequestException;
import com.demo.languages.mapper.GroupServiceMapper;
import com.demo.languages.mapper.ServicesMapper;
import com.demo.languages.repository.GroupServicesRepository;
import com.demo.languages.service.GroupServices;
import com.demo.languages.validate.ValidateAlias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;

@Service
public class GroupServicesImpl implements GroupServices {


    @Autowired
    private GroupServicesRepository repository;

    @Autowired
    private GroupServiceMapper mapper;

    @Autowired
    private ServicesMapper servicesMapper;

    @Override
    public ListDataResponse<GroupServicesOutput> findAll() {
        List<GroupServicesEntity> list = repository.findAll();
        List<GroupServicesOutput> outputList = mapper.mapListOutPut(list);
        ListDataResponse<GroupServicesOutput> listDataResponse = new ListDataResponse<>();
        listDataResponse.setContent(outputList);
        return listDataResponse;
    }

    @Override
    public DataResponse<GroupServicesOutput> findById(Integer id) {
        try {
            GroupServicesEntity groupServices = repository.findById(id).get();
            GroupServicesOutput output = mapper.mapOutPut(groupServices);
            DataResponse<GroupServicesOutput> response = new DataResponse<>();
            response.setData(output);
            response.setMessage("Success !");
            return response;
        }catch (Exception exception){
            throw new RequestException("NOT FOUND GROUP SERVICE");
        }
    }

    @Override
    public DataResponse<GroupServicesOutput> insert(GroupServicesInput input) {
        GroupServicesEntity groupServicesEntity = mapper.mapEntity(input);
        //Chỉnh alias tên viết thường
        String alias = ValidateAlias.rule(groupServicesEntity.getAlias());
        groupServicesEntity.setAlias(alias);
        groupServicesEntity.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
        repository.save(groupServicesEntity);
        GroupServicesOutput output = mapper.mapOutPut(groupServicesEntity);
        DataResponse<GroupServicesOutput> dataResponse = new DataResponse<>();
        dataResponse.setMessage("Success !");
        dataResponse.setData(output);
        return dataResponse;
    }

    @Override
    public DataResponse<GroupServicesOutput> update(Integer id, GroupServicesInput input) {
        try {
            GroupServicesEntity groupServicesEntity = repository.findById(id).get();
            String alias = ValidateAlias.rule(groupServicesEntity.getAlias());
            mapper.updateGroupService(input, groupServicesEntity);
            groupServicesEntity.setAlias(alias);
            groupServicesEntity.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(groupServicesEntity);
            GroupServicesOutput output = mapper.mapOutPut(groupServicesEntity);
            DataResponse<GroupServicesOutput> dataResponse = new DataResponse<>();
            dataResponse.setData(output);
            dataResponse.setMessage("Success !");
            return dataResponse;
        }catch (Exception exception){
             throw new RequestException("NOT FOUND GROUP SERVICE" + id);
        }
    }

    @Override
    public void delete(Integer id) {
       repository.deleteById(id);
    }

    @Override
    public void sortDelete(Integer id) {
        try {
            GroupServicesEntity groupServices = repository.findById(id).get();
            groupServices.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(groupServices);
        }catch (Exception exception){
           throw new RequestException("NOT FOUND GROUP SERVICE !");
        }
    }
}
