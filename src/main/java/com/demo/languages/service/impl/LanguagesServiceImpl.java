package com.demo.languages.service.impl;

import com.demo.languages.dto.Request.LanguagesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.LanguagesOutput;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.entities.Languages;
import com.demo.languages.exception.RequestException;
import com.demo.languages.mapper.LanguagesMapper;
import com.demo.languages.repository.LanguagesRepository;
import com.demo.languages.service.LanguagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class LanguagesServiceImpl implements LanguagesService {


    @Autowired
    private LanguagesRepository repository;


    @Autowired
    private LanguagesMapper mapper;


    @Override
    public ListDataResponse<LanguagesOutput> findAll() {
        List<Languages> list = new ArrayList<>();
        repository.findAll().forEach(
                (n) -> {
                    if (n.getDeleted_at() == null) {
                        list.add(n);
                    }
                }
        );
        List<LanguagesOutput> Outputs = mapper.mapListOutput(list);
        ListDataResponse<LanguagesOutput> listDataResponse = new ListDataResponse<>();
        listDataResponse.setContent(Outputs);
        return listDataResponse;
    }


    @Override
    public DataResponse<LanguagesOutput> findById(Integer id) {
        try {
            Languages languages = repository.findById(id).get();
            if (languages.getDeleted_at() != null) {
                throw new RequestException("NOT FOUND LANGUAGES" + id);
            }
            LanguagesOutput output = mapper.mapOutput(languages);
            DataResponse<LanguagesOutput> liDataResponse = new DataResponse<>();
            liDataResponse.setMessage("Success !");
            liDataResponse.setData(output);
            return liDataResponse;
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND LANGUAGES");
        }
    }

    @Override
    public DataResponse<LanguagesOutput> insert(LanguagesInput input) {
        try {
            Languages languages = mapper.postEntity(input);
            languages.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(languages);
            LanguagesOutput output = mapper.mapOutput(languages);
            DataResponse<LanguagesOutput> response = new DataResponse<>();
            response.setData(output);
            response.setMessage("Success !");
            return response;
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND LANGUAGES");
        }
    }

    @Override
    public DataResponse<LanguagesOutput> update(Integer id, LanguagesInput input) {
        try {
            Languages languages = repository.findById(id).get();
            mapper.updateLanguages(input, languages);
            repository.save(languages);
            LanguagesOutput output = mapper.mapOutput(languages);
            DataResponse<LanguagesOutput> response = new DataResponse<>();
            response.setData(output);
            response.setMessage("Success !");
            return response;
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND LANGUAGES" + id);
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            repository.deleteById(id);
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND LANGUAGES");
        }
    }


    @Override
    public void sortDelete(Integer id) {
        try {
            Languages languages = repository.findById(id).get();
            if (languages.getDeleted_at() != null) {
                throw new RequestException("NOT FOUND LANGUAGES");
            }
            languages.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(languages);
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND LANGUAGES");
        }
    }

    @Override
    public boolean existsByCode(String code) {
        return repository.existsByCode(code);
    }
}
