package com.demo.languages.service.impl;

import com.demo.languages.dto.Request.ServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.dto.Response.ServicesOutput;
import com.demo.languages.entities.ServicesEntity;
import com.demo.languages.exception.RequestException;
import com.demo.languages.mapper.ServicesMapper;
import com.demo.languages.repository.ServicesRepository;
import com.demo.languages.service.Services;
import com.demo.languages.validate.ValidateFirstLetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


@Service
public class ServicesImpl implements Services {


    @Autowired
    private ServicesRepository repository;


    @Autowired
    private ServicesMapper mapper;

    @Override
    public ListDataResponse<ServicesOutput> findAll() {
        ListDataResponse<ServicesOutput> listDataResponse = new ListDataResponse<>();
        List<ServicesEntity> servicesEntityList = new ArrayList<>();
        repository.findAll().forEach((n) -> {
            if (n.getDeleted_at() == null

            ) {

                servicesEntityList.add(n);


            }
        });
        List<ServicesOutput> outputList = mapper.mapListServicesOuput(servicesEntityList);
        outputList.forEach(
                (n)->{
                    n.getGroup_service_id().setServicesEntityList(null);
                }
        );
        listDataResponse.setContent(outputList);
        return listDataResponse;
    }

    @Override
    public DataResponse<ServicesOutput> findById(Integer id) {
        DataResponse<ServicesOutput> response = new DataResponse<>();
        if (repository.findById(id).get().getDeleted_at() != null) {
            throw new RequestException("NOT FOUND SERVICES !");
        }
        ServicesOutput servicesOutput = mapper.mapServicesOuput(repository.findById(id).get());
        servicesOutput.getGroup_service_id().setServicesEntityList(null);
        response.setData(servicesOutput);
        response.setMessage("Success !");
        return response;
    }

    @Override
    public DataResponse<ServicesOutput> insert(ServicesInput input) {
        Boolean check = ValidateFirstLetter.chuCaiDau(input.getPartner_code());
        if (check) {
            ServicesEntity servicesEntity = mapper.insert(input);
            servicesEntity.setCreated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(servicesEntity);
            ServicesOutput output = mapper.mapServicesOuput(servicesEntity);
            DataResponse<ServicesOutput> response = new DataResponse<>();
            response.setData(output);
            response.setMessage("Success !");
            return response;
        } else {
            throw new RequestException("Bắt buộc nhập phải bắt đầu từ chữ cái");
        }
    }

    @Override
    public DataResponse<ServicesOutput> update(Integer id, ServicesInput input) {
        try {
            Boolean check = ValidateFirstLetter.chuCaiDau(input.getPartner_code());
            if (check) {
                ServicesEntity servicesEntity = repository.findById(id).get();
                mapper.update(input, servicesEntity);
                servicesEntity.setUpdated_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
                repository.save(servicesEntity);
                ServicesOutput output = mapper.mapServicesOuput(servicesEntity);
                DataResponse<ServicesOutput> response = new DataResponse<>();
                response.setData(output);
                response.setMessage("Success !");
                return response;
            } else {
                throw new RequestException("Bắt buộc nhập phải bắt đầu từ chữ cái");
            }
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND SERVICES !");
        }
    }

    @Override
    public void delete(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public void sortDelete(Integer id) {
        try {
            ServicesEntity services = repository.findById(id).get();
            services.setDeleted_at(Timestamp.valueOf(ZonedDateTime.now().toLocalDateTime()));
            repository.save(services);
        } catch (Exception exception) {
            throw new RequestException("NOT FOUND SERVICES !");
        }
    }
}
