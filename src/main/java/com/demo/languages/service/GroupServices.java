package com.demo.languages.service;

import com.demo.languages.dto.Request.GroupServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.GroupServicesOutput;
import com.demo.languages.dto.Response.ListDataResponse;

public interface GroupServices {

    ListDataResponse<GroupServicesOutput> findAll();

    DataResponse<GroupServicesOutput> findById(Integer id);

    DataResponse<GroupServicesOutput> insert(GroupServicesInput input);

    DataResponse<GroupServicesOutput> update(Integer id, GroupServicesInput input);

    void delete(Integer id);

    void sortDelete(Integer id);
}
