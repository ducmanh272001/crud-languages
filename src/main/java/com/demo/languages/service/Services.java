package com.demo.languages.service;




import com.demo.languages.dto.Request.ServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.dto.Response.ServicesOutput;
import org.springframework.stereotype.Service;

import java.util.List;

public interface Services {

       ListDataResponse<ServicesOutput> findAll();
       DataResponse<ServicesOutput> findById(Integer id);
       DataResponse<ServicesOutput> insert(ServicesInput input);
       DataResponse<ServicesOutput> update(Integer id, ServicesInput input);
       void delete(Integer id);
       void sortDelete(Integer id);
}
