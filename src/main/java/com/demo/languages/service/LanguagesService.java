package com.demo.languages.service;

import com.demo.languages.dto.Request.LanguagesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.LanguagesOutput;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.entities.Languages;

import java.util.List;

public interface LanguagesService {
    ListDataResponse<LanguagesOutput> findAll();
    DataResponse<LanguagesOutput> findById(Integer id);
    DataResponse<LanguagesOutput> insert(LanguagesInput input);
    DataResponse<LanguagesOutput> update(Integer id, LanguagesInput input);
    void delete(Integer id);
    void sortDelete(Integer id);

    boolean existsByCode(String code);
}
