package com.demo.languages.controller;


import com.demo.languages.dto.Request.LanguagesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.LanguagesOutput;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.exception.RequestException;
import com.demo.languages.service.LanguagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/languages-demo")
public class LanguagesController {

    @Autowired
    private LanguagesService service;


    @GetMapping()
    public ListDataResponse<LanguagesOutput> getList() {
        return service.findAll();
    }

    @GetMapping(value = "/{id}")
    public DataResponse<LanguagesOutput> getById(@PathVariable(value = "id") Integer id) {
        return service.findById(id);
    }


    @PostMapping()
    public DataResponse<LanguagesOutput> insert(@RequestBody @Valid LanguagesInput input) throws Exception {
        if (service.existsByCode(input.getCode())) {
            throw new RequestException("Code đã tồn tại");
        }
        return service.insert(input);
    }

    @DeleteMapping("/sortDelete/{id}")
    public void sortDelete(@PathVariable(value = "id") Integer id) {
        service.sortDelete(id);
    }


    @PutMapping("/{id}")
    public DataResponse<LanguagesOutput> update(@PathVariable(value = "id") Integer id, @RequestBody @Valid LanguagesInput input) {
        if (service.existsByCode(input.getCode())) {
            throw new RequestException("Username đã tồn tại");
        }
        return service.update(id, input);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") Integer id) {
        service.delete(id);
    }

}
