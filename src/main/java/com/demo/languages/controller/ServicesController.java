package com.demo.languages.controller;


import com.demo.languages.dto.Request.ServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.dto.Response.ServicesOutput;
import com.demo.languages.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/services")
public class ServicesController {

    @Autowired
    private Services services;

    @GetMapping
    public ListDataResponse<ServicesOutput> getList() {
        return services.findAll();
    }

    @GetMapping(value = "/{id}")
    public DataResponse<ServicesOutput> getById(@PathVariable(value = "id") Integer id) {
        return services.findById(id);
    }

    @PostMapping
    public DataResponse<ServicesOutput> insert(@RequestBody ServicesInput input) {
        return services.insert(input);
    }

    @DeleteMapping("/sortDelete/{id}")
    public void sortDelete(@PathVariable(value = "id") Integer id) {
        services.sortDelete(id);
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id") Integer id) {
        services.delete(id);
    }


    @PutMapping("/{id}")
    public DataResponse<ServicesOutput> update(@PathVariable(value = "id")Integer id, @RequestBody ServicesInput input){
        return services.update(id, input);
    }
}
