package com.demo.languages.controller;


import com.demo.languages.dto.Request.GroupServicesInput;
import com.demo.languages.dto.Response.DataResponse;
import com.demo.languages.dto.Response.GroupServicesOutput;
import com.demo.languages.dto.Response.ListDataResponse;
import com.demo.languages.service.GroupServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/v1/group")
public class GroupServicesController {


    @Autowired
    private GroupServices services;

    @GetMapping
    ListDataResponse<GroupServicesOutput> getList(){
        return services.findAll();
    }

    @GetMapping(value = "/{id}")
    DataResponse<GroupServicesOutput> getById(@PathVariable(value = "id")Integer id){
        return services.findById(id);
        //sdsdsffsfsfs
    }

    @PostMapping
    DataResponse<GroupServicesOutput> insert(@RequestBody @Valid GroupServicesInput input){
        return services.insert(input);
    }

    @PutMapping("/{id}")
    DataResponse<GroupServicesOutput> update(@PathVariable(value = "id")Integer id, @RequestBody @Valid GroupServicesInput input){
          return services.update(id, input);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(value = "id")Integer id){
         services.delete(id);
    }

    @DeleteMapping("/sortDelete/{id}")
    public void sortDelete(@PathVariable(value = "id")Integer id){
        services.sortDelete(id);
    }
}
