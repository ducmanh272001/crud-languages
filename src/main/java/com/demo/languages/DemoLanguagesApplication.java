package com.demo.languages;

import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoLanguagesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLanguagesApplication.class, args);
	}


	@Bean
	public OpenAPI openApiConfig() {
		return new OpenAPI().info(apiInfo());
	}

	public Info apiInfo() {
		Info info = new Info();
		info
				.title("Demo Languages API")
				.description("Demo CRUD Languages API")
				.version("v1.0.0");
		return info;
	}

}
