package com.demo.languages.mapper;


import com.demo.languages.dto.Request.GroupServicesInput;
import com.demo.languages.dto.Response.GroupServicesOutput;
import com.demo.languages.entities.GroupServicesEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GroupServiceMapper {



    List<GroupServicesOutput> mapListOutPut(List<GroupServicesEntity> list);


    @Mapping(source = "servicesEntityList",target = "service_id")
    GroupServicesOutput mapOutPut(GroupServicesEntity entity);



    GroupServicesEntity mapEntity(GroupServicesInput input);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateGroupService(GroupServicesInput input, @MappingTarget GroupServicesEntity entity);


}
