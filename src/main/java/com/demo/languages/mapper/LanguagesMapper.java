package com.demo.languages.mapper;


import com.demo.languages.dto.Request.LanguagesInput;
import com.demo.languages.dto.Response.LanguagesOutput;
import com.demo.languages.entities.Languages;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LanguagesMapper {
    LanguagesOutput mapOutput(Languages languages);

    Languages mapEntity(LanguagesOutput languagesOutput);

    List<LanguagesOutput> mapListOutput(List<Languages> languagesList);

    //Chuyển từ entiy sang input
    LanguagesInput mapInput(Languages languages);

    //Chuyển từ in put sang entity


    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Languages postEntity(LanguagesInput languagesInput);


    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateLanguages(LanguagesInput input, @MappingTarget Languages languages);


//    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
//    void update(Languages languages, LanguagesInput input);




}
