package com.demo.languages.mapper;


import com.demo.languages.dto.Request.ServicesInput;
import com.demo.languages.dto.Response.GroupServicesOutput;
import com.demo.languages.dto.Response.ServicesOutput;
import com.demo.languages.entities.GroupServicesEntity;
import com.demo.languages.entities.ServicesEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ServicesMapper {


    List<ServicesOutput> mapListServicesOuput(List<ServicesEntity> servicesEntityList);



    @Mapping(source = "groupServicesEntity",target = "group_service_id")
    ServicesOutput mapServicesOuput(ServicesEntity servicesEntity);

    @Mapping(source = "group_service_id",target = "groupServicesEntity")
    ServicesEntity mapServicesEntity(ServicesOutput output);


    @Mapping(source = "group_service_id",target = "groupServicesEntity.id")
    ServicesEntity insert(ServicesInput input);




    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(source = "group_service_id",target = "groupServicesEntity.id")
    void update(ServicesInput input, @MappingTarget ServicesEntity servicesEntity);
}
