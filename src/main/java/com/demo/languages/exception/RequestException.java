package com.demo.languages.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RequestException extends RuntimeException {


    public RequestException(String message) {
        super(message);
    }
}
