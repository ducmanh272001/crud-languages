package com.demo.languages.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;


@AllArgsConstructor
@Getter
@Setter
public class ExceptionHandle {

    private final String message;

    private final HttpStatus httpStatus;

    private final Timestamp timestamp;


}
