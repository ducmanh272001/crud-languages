package com.demo.languages.validate;

import java.util.regex.Pattern;

public class ValidateFirstLetter {


    private ValidateFirstLetter(){

    }

    public static Boolean chuCaiDau(String PartnerCode){
        String Partner_code = PartnerCode.substring(0, 1);
        Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
        if (pattern.matcher(Partner_code).find()) {
            return true;
        }else {
            return false;
        }
    }
}
