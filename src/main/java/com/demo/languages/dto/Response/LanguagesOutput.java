package com.demo.languages.dto.Response;


import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LanguagesOutput {

    private Integer id;

    private String flag;

    private String code;

    private String name;

    private String description;
}
