package com.demo.languages.dto.Response;


import com.demo.languages.entities.GroupServicesEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ServicesOutput {
    private Integer id;
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String partner_code;
    private Float price;
    private String desription;
    private GroupServicesEntity group_service_id;
}
