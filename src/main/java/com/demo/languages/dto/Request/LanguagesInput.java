package com.demo.languages.dto.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LanguagesInput {

    @NotNull(message = "Không được để trống")
    private String flag;
    @Pattern(regexp = "^[a-zA-Z0-9 ]{0,20}+$", message = "Không chứa kí tự đặc biệt")
    private String code;
    @Size(min = 1, max = 250, message = "Name không vượt quá 250 kí tự")
    private String name;
    private String description;
    private Timestamp created_at;
}
