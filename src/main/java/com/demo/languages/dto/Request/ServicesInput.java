package com.demo.languages.dto.Request;


import com.demo.languages.entities.GroupServicesEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ServicesInput {
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String partner_code;
    private Float price;
    private String desription;
    private Integer group_service_id;
}
