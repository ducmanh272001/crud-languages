package com.demo.languages.dto.Request;

import com.demo.languages.dto.Response.ServicesOutput;
import com.demo.languages.entities.ServicesEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GroupServicesInput {
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String desription;
    private Boolean is_active;
}
