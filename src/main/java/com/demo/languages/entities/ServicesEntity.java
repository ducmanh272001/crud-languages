package com.demo.languages.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "services")
public class ServicesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String partner_code;
    private Float price;
    private String desription;
    private Integer created_by;
    private Integer updated_by;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "group_service_id", referencedColumnName = "id")
    private GroupServicesEntity groupServicesEntity;
}
