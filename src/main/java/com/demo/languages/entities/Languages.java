package com.demo.languages.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@Entity
@Table(name = "languages")
public class Languages {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String flag;
    private String code;
    private String name;
    private String description;
    private Integer created_by;
    private Integer updated_by;
    private Timestamp created_at;
    private Timestamp updated_at;
    private Timestamp deleted_at;
}
