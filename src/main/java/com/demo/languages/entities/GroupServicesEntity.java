package com.demo.languages.entities;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "group_services")
public class GroupServicesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String name;
    private String alias;
    private String image_url;
    private String desription;
    private Boolean is_active;
    private Timestamp created_at;
    private Timestamp updated_at ;
    private Timestamp deleted_at;
    @OneToMany(mappedBy = "groupServicesEntity", fetch = FetchType.EAGER)
    private List<ServicesEntity> servicesEntityList;
}
