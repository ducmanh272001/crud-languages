package com.demo.languages.repository;

import com.demo.languages.entities.GroupServicesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface GroupServicesRepository extends JpaRepository<GroupServicesEntity, Integer> {
}
