package com.demo.languages.repository;

import com.demo.languages.entities.Languages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LanguagesRepository extends JpaRepository<Languages, Integer> {
    boolean existsByCode(String code);
}
